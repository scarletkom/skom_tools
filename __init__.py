# BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# END GPL LICENSE BLOCK #####

bl_info = {
    "name": "Skom Tools",
    "author": "@scarletkom",
    "version": (0, 0, 2),
    "blender": (2, 79, 0),
    "location": "3D Viewport",
    "description": "Skom Tools",
    "warning": "",
    "wiki_url": "https://gitlab.com/scarletkom/skom_tools/wikis/home",
    "tracker_url": "https://gitlab.com/scarletkom/skom_tools/issues",
    "category": "Tools"}

if "bpy" in locals():
    import imp
    imp.reload(skom_MESH_MT_shape_key_specials)
    imp.reload(skom_VIEW3D_MT_edit_mesh_specials)
else:
    from . import skom_MESH_MT_shape_key_specials
    from . import skom_VIEW3D_MT_edit_mesh_specials

import bpy

# アドオン登録


def register():
    bpy.utils.register_module(__name__)
    bpy.types.MESH_MT_shape_key_specials.append(
        skom_MESH_MT_shape_key_specials.menu_func)
    bpy.types.VIEW3D_MT_edit_mesh_specials.append(
        skom_VIEW3D_MT_edit_mesh_specials.menu_func)

    bpy.types.Scene.shape_key_select_item_list = bpy.props.CollectionProperty(
        type=skom_MESH_MT_shape_key_specials.ShapeKeySelectItem)
    bpy.types.Scene.active_index = bpy.props.IntProperty(
        name="Index for shape_key_item_list", default=0)

# アドオン登録解除


def unregister():
    del bpy.types.Scene.shape_key_select_item_list
    del bpy.types.Scene.active_index

    bpy.types.MESH_MT_shape_key_specials.remove(
        skom_MESH_MT_shape_key_specials.menu_func)
    bpy.types.VIEW3D_MT_edit_mesh_specials.remove(
        skom_VIEW3D_MT_edit_mesh_specials.menu_func)

    bpy.utils.unregister_module(__name__)


if __name__ == "__main__":
    register()
