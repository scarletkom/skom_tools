# BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# END GPL LICENSE BLOCK #####

# 「3Dビュー」エリア → メッシュ編集モード → 「W」キー
import os
import re
import sys
import bpy
import time
import bmesh
import mathutils

# メニュー等に項目追加


def menu_func(self, context):
    self.layout.separator()
    self.layout.operator('mesh.selected_mesh_sewing',
                         text="選択した頂点を縫い合わせる",  icon='MESH_PLANE')


class selected_mesh_sewing(bpy.types.Operator):
    bl_idname = 'mesh.selected_mesh_sewing'
    bl_label = "選択した頂点を縫い合わせる"
    bl_description = "選択中の頂点を選択順に奇数の頂点座標に偶数の頂点座標を合わせます"
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        return ob.type == 'MESH'

    def execute(self, context):
        ob = context.active_object
        if ob.mode != 'EDIT':
            self.report(type={'ERROR'}, message="エディットモードで実行してください")
            return {'CANCELLED'}
        me = ob.data

        pre_mode = ob.mode
        bpy.ops.object.mode_set(mode='OBJECT')

        bm = bmesh.new()
        bm.from_mesh(me)

        vect_list = []
        for h in bm.select_history:
            vect_list.append(h.index)

        if me.shape_keys:
            for shape_key in me.shape_keys.key_blocks:
                for i in range((int)(len(vect_list) / 2)):
                    shape_key.data[vect_list[i * 2 + 1]
                                   ].co = shape_key.data[vect_list[i * 2]].co

        else:
            bm.verts.ensure_lookup_table()

            for i in range((int)(len(vect_list) / 2)):
                bm.verts[vect_list[i * 2 + 1]
                         ].co = bm.verts[vect_list[i * 2]].co
            bm.to_mesh(me)

        bpy.ops.object.mode_set(mode=pre_mode)

        return {'FINISHED'}
