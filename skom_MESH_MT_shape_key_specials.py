# BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# END GPL LICENSE BLOCK #####

# 「プロパティ」エリア → 「メッシュデータ」タブ → 「シェイプキー」パネル → ▼ボタン
import os
import re
import sys
import bpy
import time
import bmesh
import mathutils

# メニュー等に項目追加


def menu_func(self, context):
    self.layout.separator()
    self.layout.operator(
        'object.selected_shape_key_transfer', icon='MESH_PLANE')


class ShapeKeySelectItem(bpy.types.PropertyGroup):
    value = bpy.props.BoolProperty(
        name="Basis",
        default=False)


class ShapeKeySelectItemList(bpy.types.UIList):
    def draw_item(
            self, context, layout, data, item, icon, active_data,
            active_propname, index):

        item_icon = 'SHAPEKEY_DATA'

        # Make sure your code supports all 3 layout types
        if self.layout_type in {'DEFAULT', 'COMPACT'}:
            layout.prop(item, "value", text=item.name)

        elif self.layout_type in {'GRID'}:
            layout.alignment = 'CENTER'
            layout.label("", icon=custom_icon)


class ShapeKeySelectItemDeselectAll(bpy.types.Operator):
    bl_idname = "shape_key_select_item_list.deselect_all"
    bl_label = "Deselect All"

    def check(self, context):
        return True

    def execute(self, context):
        selected = False
        for item in context.scene.shape_key_select_item_list:
            selected = selected or item.value

        for item in context.scene.shape_key_select_item_list:
            item.value = not selected

        context.area.tag_redraw()

        return{'FINISHED'}


class SelectedShapeKeyTransfer(bpy.types.Operator):
    bl_idname = 'object.selected_shape_key_transfer'
    bl_label = "選択した頂点のシェイプキー転送"
    bl_description = "メッシュの選択した頂点のシェイプキーを選択順に転送します"
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        active_ob = context.active_object
        obs = context.selected_objects
        if len(obs) != 2:
            return False
        for ob in obs:
            if ob.type != 'MESH':
                return False
        bm0 = bmesh.new()
        bm0.from_mesh(obs[0].data)
        bm1 = bmesh.new()
        bm1.from_mesh(obs[1].data)
        if len(bm0.select_history) == len(bm1.select_history) and len(bm0.select_history) > 0:
            return True
        return False

    def invoke(self, context, event):
        target_ob = context.active_object
        target_me = target_ob.data

        for ob in context.selected_objects:
            if ob.name != target_ob.name:
                source_ob = ob
                break

        source_me = source_ob.data
        target_me = target_ob.data

        previous_select_shape_keys = []
        for item in context.scene.shape_key_select_item_list:
            if item.value == True:
                previous_select_shape_keys.append(item.name)

        context.scene.shape_key_select_item_list.clear()
        for source_shape_key_index, source_shape_key in enumerate(source_me.shape_keys.key_blocks):
            if target_me.shape_keys:
                if source_shape_key.name in target_me.shape_keys.key_blocks:
                    select_item = context.scene.shape_key_select_item_list.add()
                    select_item.name = source_shape_key.name
                    if select_item.name in previous_select_shape_keys:
                        select_item.value = True

        return context.window_manager.invoke_props_dialog(self)

    def draw(self, context):
        layout = self.layout
        obj = context.object
        row = layout.row()
        row.template_list("ShapeKeySelectItemList", "", context.scene, "shape_key_select_item_list", context.scene,
                          "active_index", rows=5)
        row = layout.row()
        row.operator("shape_key_select_item_list.deselect_all")

    def execute(self, context):
        import mathutils
        import time
        start_time = time.time()

        target_ob = context.active_object
        target_me = target_ob.data
        target_shape_key_index = target_ob.active_shape_key_index

        pre_mode = target_ob.mode
        bpy.ops.object.mode_set(mode='OBJECT')

        for ob in context.selected_objects:
            if ob.name != target_ob.name:
                source_ob = ob
                break

        context.window_manager.progress_begin(0, 1)
        context.window_manager.progress_update(0)

        select_shape_keys = []
        for item in context.scene.shape_key_select_item_list:
            if item.value == True:
                select_shape_keys.append(item.name)

        source_me = source_ob.data
        vect_list = []
        source_bm = bmesh.new()
        source_bm.from_mesh(source_me)
        for s in source_bm.select_history:
            vect_list.append(s.index)
        target_me = target_ob.data
        target_bm = bmesh.new()
        target_bm.from_mesh(target_me)
        for source_shape_key_index, source_shape_key in enumerate(source_me.shape_keys.key_blocks):
            i = 0
            if target_me.shape_keys:
                if source_shape_key.name in target_me.shape_keys.key_blocks and source_shape_key.name in select_shape_keys:
                    target_shape_key = target_me.shape_keys.key_blocks[source_shape_key.name]
                    for s in target_bm.select_history:
                        target_shape_key.data[s.index].co = source_shape_key.data[vect_list[i]].co
                        i += 1

        context.window_manager.progress_end()

        target_ob.active_shape_key_index = 0
        context.scene.objects.active = target_ob
        bpy.ops.object.mode_set(mode=pre_mode)

        diff_time = time.time() - start_time
        self.report(type={'INFO'}, message=str(
            round(diff_time, 1)) + " Seconds")
        return {'FINISHED'}
